package blackjack

import scala.language.implicitConversions

object CardRank extends Enumeration {
  type CardRank = Val

  protected case class Val(val symbol: String, val value: Int) extends super.Val {
  }
  implicit def valueToRank(x: Value) = x.asInstanceOf[Val]

  val TWO = Val("2", 2)
  val THREE = Val("3", 3)
  val FOUR = Val("4", 4)
  val FIVE = Val("5", 5)
  val SIX = Val("6", 6)
  val SEVEN = Val("7", 7)
  val EIGHT = Val("8", 8)
  val NINE = Val("9", 9)
  val TEN = Val("10", 10)
  val JACK = Val("J", 10)
  val QUEEN = Val("Q", 10)
  val KING = Val("K", 10)
  val ACE = Val("A", 11)

}