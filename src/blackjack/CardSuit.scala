package blackjack

import scala.language.implicitConversions

import java.awt.Color

object CardSuit extends Enumeration {
  type CardSuit = Val

  protected case class Val(val name: String, val symbol: Char) extends super.Val {
    def color: Color = {
      if (name.equals("hearts") || name.equals("diamonds"))
        Color.RED
      else
        Color.BLACK
    }
  }
  implicit def valueToSuit(x: Value) = x.asInstanceOf[Val]

  val SPADES = Val("spades", '\u2660')
  val HEARTS = Val("hearts", '\u2665')
  val DIAMONDS = Val("diamonds", '\u2666')
  val CLUBS = Val("clubs", '\u2663')
}