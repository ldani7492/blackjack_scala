package blackjack

import scala.collection.mutable.ListBuffer
import scala.swing.Swing
import javax.swing.Timer

@SerialVersionUID(3934422413677103558L)
class Table(private val id: String, private val decks: Int) extends Serializable {

  private val deck = new Deck
  private val _dealer = new Player("dealer")
  private val players = ListBuffer.empty[Player]
  private val playing = ListBuffer.empty[Player]

  private var iterator: Iterator[Player] = null
  private var _activePlayer: Player = null

  private var _over = true
  private var _holeRevealed = false

  private val newGameTimer = new Timer(60000, Swing.ActionListener(ea => {
    newGame
  }))
  private val timeout = new Timer(60000, Swing.ActionListener(ea => {
    try {
      leave(_activePlayer)
    } catch {
      case e: Exception => e.printStackTrace
    }
  }))

  def hit(p: Player): Unit = {
    if (p.active) {
      timeout.restart
      val player: Player = pickHand(p)
      try {
        player.drawCard(deck.draw)
        println(player.id + " hit.")
      } catch {
        case ede: EmptyDeckException => {
          println("The deck is empty")
          ede.printStackTrace
        }
      }
      if (player.points > 21) {
        bust(player)
        println(player.id + " busted.")
      }
      if (player.points == 21) {
        stand(player)
        println(player.id + " has blackjack.")
      }
    }
  }

  def stand(p: Player): Unit = {
    if (p.active) {
      timeout.restart
      val player: Player = pickHand(p)
      println(player.id + " stood.")
      if (!checkSideHand(p)) {
        player.active = false
        nextPlayer
      }
    }
  }

  @throws(classOf[InsufficientFundsException])
  def doubleDown(p: Player): Unit = {
    val player: Player = pickHand(p)
    if (player.active) {
      timeout.restart
      println(player.id + " doubled.")
      val bet = player.bet
      player.bet(bet)
      try {
        player.drawCard(deck.draw)
      } catch {
        case ede: EmptyDeckException => {
          println("The deck is empty")
          ede.printStackTrace
        }
      }
      if (!checkSideHand(player)) {
        player.active = false
        nextPlayer
      }
    }
  }

  @throws(classOf[InsufficientFundsException])
  def split(player: Player): Unit = {
    if (player.active
      && player.hand.size == 2
      && !player.playingSide
      && (player.hand.get(0).value == player.hand.get(1).value)) {
      timeout.restart
      println(player.id + " split.")
      player.split(deck)
      if (player.points == 21) {
        stand(player)
        println(player.id + " has blackjack.")
      }
    }
  }

  def surrender(player: Player): Unit = {
    if (player.active && player.hand.size == 2
      && !player.playingSide) {
      timeout.restart
      println(player.id + " surrendered.")
      player.active = false
      val bet = player.bet
      try {
        player.bet(-(bet / 2))
      } catch {
        case ife: InsufficientFundsException => {
          ife.printStackTrace
        }
      }
      bust(player)
      nextPlayer
    }
  }

  @throws(classOf[InsufficientFundsException])
  def insurance(player: Player, bet: Int): Unit = {
    if (player.active && player.hand.size == 2
      && dealer.hand.get(0).rank == CardRank.ACE
      && player.insurance == 0 && !player.playingSide) {
      timeout.restart
      println(player.id + " insured.")
      player.insurance(bet)
    }
  }

  def bust(p: Player): Unit = {
    p.busted = true
    val player = pickHand(p)
    if (!checkSideHand(player)) {
      nextPlayer
    }
  }

  def dealerTurn: Unit = {
    timeout.stop
    _holeRevealed = true
    val dealerHand = dealer.hand
    val c1 = dealerHand.get(0)
    val c2 = dealerHand.get(1)
    val soft17 = ((c1.rank == CardRank.ACE && c2.rank == CardRank.SIX) ||
      (c1.rank == CardRank.SIX && c2.rank == CardRank.ACE))
    while (dealer.points < 17 || (dealer.points == 17 && soft17)) {
      try {
        dealer.drawCard(deck.draw)
      } catch {
        case ede: EmptyDeckException => {
          println("The deck is empty")
          ede.printStackTrace
        }
      }
    }
    gameOver
  }

  def newGame: Unit = {
    if (over && players.size != 0 && ready) {
      newGameTimer.stop
      resetTable

      setPlaying

      prepareDeck

      println("New game started")

      deal

      timeout.start

      nextPlayer

    }
  }

  private def deal: Unit = {
    try {
      (1 to 2) foreach (x => {
        for (player <- players) {
          player.drawCard(deck.draw)
        }
        dealer.drawCard(deck.draw)
      })
    } catch {
      case ede: EmptyDeckException => ede.printStackTrace
    }
  }

  private def prepareDeck: Unit = {
    for (suit <- CardSuit.values)
      for (rank <- CardRank.values)
        (1 to decks) foreach (x => {
          deck.add(new Card(suit, rank))
        })
    deck.shuffle
  }

  private def setPlaying: Unit = {
    for (player <- players) {
      player.evaluated = false
      if (!player.playing)
        leave(player)
      else
        playing += player
    }
    iterator = playing.iterator
  }

  private def resetTable: Unit = {
    _over = false
    _holeRevealed = false
    deck.clear
    for (player <- players) {
      player.throwAll
      player.busted = false
    }
    dealer.throwAll

    playing.clear
  }

  def evaluate(player: Player): Int = {
    newGameTimer.restart
    player.evaluated = true
    var wins = 0

    wins = evaluateInsurance(player, wins)

    wins = evaluateSide(player, wins)

    wins = evaluateMain(player, wins)

    wins
  }

  private def evaluateMain(player: Player, winnings: Int): Int = {
    var wins = winnings
    val mainBet = player.bet
    if (player.points == 21 && dealer.points == 21) {
      val bet = player.bet
      try {
        player.bet(-bet)
      } catch {
        case ife: InsufficientFundsException => ife.printStackTrace
      }
    } else if (player.points == 21) {
      wins += (mainBet * 1.5).round.toInt
      player.win(true)
    } else if (player.points > 21) {
      wins -= mainBet
      player.lose
    } else if (dealer.points == 21) {
      wins -= mainBet
      player.lose
    } else if (dealer.points > 21) {
      wins += mainBet
      player.win(false)
    } else if (player.points > dealer.points
      && !player.busted) {
      wins += mainBet
      player.win(false)
    } else {
      wins -= mainBet
      player.lose
    }
    wins
  }

  private def evaluateSide(player: Player, winnings: Int): Int = {
    var wins = winnings
    val sideBet = player.sideHand.bet
    if (player.playingSide) {
      val side = player.sideHand
      if (side.points == 21 && dealer.points == 21) {
        val bet = side.bet
        try {
          side.bet(-bet)
        } catch {
          case ife: InsufficientFundsException => ife.printStackTrace
        }
      } else if (side.points == 21) {
        wins += sideBet
        player.winSide
      } else if (side.points > 21) {
        wins -= sideBet
        player.loseSide
      } else if (dealer.points == 21) {
        wins -= sideBet
        player.loseSide
      } else if (dealer.points > 21) {
        wins += sideBet
        player.winSide
      } else if (side.points > dealer.points) {
        wins += sideBet
        player.winSide
      } else {
        wins -= sideBet
        player.loseSide
      }
    }
    wins
  }

  private def evaluateInsurance(player: Player, winnings: Int): Int = {
    var wins = winnings
    def insurance = player.insurance
    def dealerHand = dealer.hand
    if (dealerHand.get(0).rank == CardRank.ACE
      && dealerHand.get(1).value == 10) {
      wins += insurance * 2
      player.winInsurance
    } else {
      wins -= insurance
      player.loseInsurance
    }
    wins
  }

  def nextPlayer: Unit = {
    if (iterator.hasNext) {
      val next = iterator.next
      if (!next.inSession || !next.playing)
        nextPlayer
      else {
        next.active = true
        _activePlayer = next
        timeout.restart
      }
    } else {
      dealerTurn
    }
  }

  def checkSideHand(player: Player): Boolean = {
    if (!player.isSide) {
      val playingSide = (player.sideHand.playing
        && !player.sideHand.active)
      if (playingSide) {
        player.sideHand.active = true
        if (player.sideHand.points == 21) {
          stand(player)
        }
      }
      playingSide
    } else
      false
  }

  def dealerPoints: Int = {
    if (holeRevealed)
      dealer.points
    else if (dealer.hand.size > 0)
      dealer.hand.get(0).value
    else
      0
  }

  @throws(classOf[TableFullException])
  def join(player: Player): Unit = {
    if (players.size < 5) {
      player.playing = false
      player.active = false
      players += player
    } else
      throw new TableFullException
  }

  def leave(player: Player): Unit = {
    if (player.active) {
      bust(player)
      if (player.playingSide) {
        bust(player)
      }
    }
    player.playing = false
    player.closeSession
    players -= player
    newGame
  }

  @throws(classOf[InsufficientFundsException])
  def stay(player: Player, bet: Integer): Unit = {
    player.bet(bet)
    player.playing = true
    newGame
  }

  def ready: Boolean = {
    var ready = true
    for (player <- players) {
      if (!player.playing)
        ready = false
    }
    ready
  }

  def playerIdList: Array[String] = {
    val play = ListBuffer.empty[String]
    for (player <- playing) {
      play += player.id
    }
    play.toArray
  }

  def gameOver(): Unit = {
    _over = true
    for (player <- playing) {
      player.playing = false
    }
  }

  def pickHand(player: Player): Player = {
    if (!player.sideActive)
      player
    else
      player.sideHand
  }

  def over = _over
  def holeRevealed = _holeRevealed
  def dealer = _dealer
  def activePlayer = _activePlayer

}