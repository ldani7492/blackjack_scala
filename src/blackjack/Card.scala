package blackjack

import java.awt.Color
import CardSuit._
import CardRank._

@SerialVersionUID(-1231107613261828543L)
class Card(private val _suit: CardSuit, private val _rank: CardRank) extends Serializable {

  def suit = _suit
  def rank = _rank
  def color = suit.color
  def value = rank.value
  override def toString = suit.symbol + rank.symbol
}