package blackjack

@SerialVersionUID(6556267046527282211L)
class Player(private val _id: String, private val _isSide: Boolean) extends Serializable {
  private var _points = 0
  private var aces = 0

  private val _hand = new Deck

  private var _inSession = false
  private var _playing = false
  private var _active = false
  private var _evaluated = false
  private var _busted = false

  private var _money = 1000
  private var _bet = 0
  private var _insurance = 0

  private var _sideHand: Player = null

  if (!isSide) {
    _sideHand = new Player("side", true)
    sideHand.money = 0
  }

  def this(_id: String) = this(_id, false)

  def throwAll: Unit = {
    hand.clear
    if (!isSide) {
      sideHand.throwAll
      sideHand.playing = false
    }
    _points = 0
    aces = 0
  }

  def reset: Unit = {
    throwAll
    _bet = 0
    _insurance = 0
    _inSession = false
    active = false
    playing = false
    evaluated = false
    busted = false
    if (!isSide)
      sideHand.reset
  }

  def drawCard(card: Card): Unit = {
    hand.add(card);
    if (card.rank == CardRank.ACE)
      aces = aces + 1
    _points += card.value

    while (points > 21 && aces > 0) {
      _points -= 10
      aces -= 1
    }
  }

  @throws(classOf[InsufficientFundsException])
  def bet(amount: Int): Unit = {
    if (amount > money)
      throw new InsufficientFundsException
    _bet += amount
    money -= amount
  }

  @throws(classOf[InsufficientFundsException])
  def sideBet: Unit = {
    if (bet > money)
      throw new InsufficientFundsException
    sideHand.money = bet
    sideHand.bet(bet)
    money -= bet
  }

  @throws(classOf[InsufficientFundsException])
  def insurance(amount: Int): Unit = {
    if (amount > money || amount > bet * 1.5)
      throw new InsufficientFundsException
    _insurance += amount
    money -= amount
  }

  @throws(classOf[InsufficientFundsException])
  def split(deck: Deck): Unit = {
    try {
      sideBet
      val card = hand.draw
      if (card.rank == CardRank.ACE) {
        aces -= 1
        _points -= 1
      } else
        _points -= card.value
      sideHand.drawCard(card)
      drawCard(deck.draw)
      sideHand.drawCard(deck.draw)
      sideHand.playing = true
    } catch {
      case ede: EmptyDeckException => ede.printStackTrace
    }
  }

  def lose: Unit = {
    _bet = 0;
  }

  def win(blackjack: Boolean): Unit = {
    if (blackjack)
      _money += (bet * 2.5).round.toInt;
    else
      money += bet * 2;
    _bet = 0;
  }

  def loseInsurance: Unit = {
    _insurance = 0
  }

  def winInsurance: Unit = {
    money += bet * 3;
    _bet = 0;
  }

  def loseSide: Unit = {
    sideHand.lose
  }

  def winSide: Unit = {
    val bet = sideHand.bet
    sideHand.lose
    money += bet * 2;
  }

  def playingSide: Boolean = {
    if (!isSide)
      sideHand.playing
    else
      false
  }

  def sideActive(): Boolean = {
    if (!isSide)
      sideHand.active
    else
      false
  }

  def id = _id

  def points = _points

  def hand = _hand

  def inSession = _inSession
  def startSession = { _inSession = true }
  def closeSession = { _inSession = false }

  def playing = _playing
  def playing_=(b: Boolean): Unit = { _playing = b }

  def active = _active
  def active_=(b: Boolean): Unit = { _active = b }

  def evaluated = _evaluated
  def evaluated_=(b: Boolean): Unit = { _evaluated = b }

  def isSide = _isSide

  def busted = _busted
  def busted_=(b: Boolean): Unit = { _busted = b }

  def money = _money
  def money_=(amount: Int): Unit = { _money = amount }

  def bet = _bet

  def insurance = _insurance

  def sideHand = _sideHand

  override def toString = {
    hand.toString
  }
}