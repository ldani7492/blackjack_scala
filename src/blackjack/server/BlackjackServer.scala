package blackjack.server

import java.rmi.RemoteException
import blackjack.Player
import scala.collection.mutable.HashMap
import blackjack.InsufficientFundsException
import blackjack.Deck
import scala.swing.Swing
import javax.swing.Timer
import scala.xml.XML
import java.rmi.server.UnicastRemoteObject
import java.rmi.registry.LocateRegistry
import blackjack.TableFullException
import blackjack.Table

object BlackjackServer {
  def main(args: Array[String]): Unit = {

    try {
      val name = "BlackjackServer"
      val server: ServerInterface = new BlackjackServer
      val stub = UnicastRemoteObject.exportObject(server, 0).asInstanceOf[ServerInterface]
      val registry = LocateRegistry.createRegistry(1099)
      registry.rebind(name, stub)
      println("BlackjackServer started")
    } catch {
      case re: RemoteException => {
        println("Could not start server")
        re.printStackTrace
      }
    }

  }
}

class BlackjackServer extends ServerInterface {

  private val tables = HashMap.empty[String, Table]
  private val players = HashMap.empty[String, Player]

  readTables
  readPlayers

  private val saveTimer = new Timer(60000, Swing.ActionListener(ea => {
    savePlayers
  }))

  saveTimer.start

  def readTables: Unit = {
    try {
      val xml = XML.loadFile("src/blackjack/server/tables.xml")
      val entries = xml \ "tables" \ "entry"

      entries.map(entry => {
        val key = (entry \ "key").text
        val decks = (entry \ "value" \ "decks").text.toInt
        val id = (entry \ "value" \ "id").text

        tables.put(key, new Table(id, decks))
      })

      println("Tables loaded.")
    } catch {
      case e: Exception =>
        println("Error loading tables.")
        System.exit(0)
    }
  }

  def readPlayers: Unit = {
    try {
      val xml = XML.loadFile("src/blackjack/server/players.xml")
      val entries = xml \ "players" \ "entry"

      entries.map(entry => {
        val key = (entry \ "key").text
        val money = (entry \ "value" \ "money").text.toInt
        val id = (entry \ "value" \ "id").text

        val player = new Player(id)
        player.money = money
        players.put(key, player)
      })

      println("Players loaded.")
    } catch {
      case e: Exception => println("Error loading players, creating new database.")
    }
  }

  def savePlayers: Unit = {
    def mapPlayers = {
      players.map(entry => {
        val (key, player) = entry
        val id = player.id
        val money = player.money

        (<entry>
           <key>{ key }</key>
           <value>
             <id>{ id }</id>
             <money>{ money }</money>
           </value>
         </entry>)
      })
    }

    val xml = (
      <players>
        <players>
          { mapPlayers }
        </players>
      </players>)

    XML.save("src/blackjack/server/players.xml", xml)

    println("Players saved.")
  }

  @throws(classOf[RemoteException])
  def hit(playerId: String, tableId: String): Unit = {
    val player = players.apply(playerId)
    val table = tables.apply(tableId)
    table.hit(player)
  }

  @throws(classOf[RemoteException])
  def stand(playerId: String, tableId: String): Unit = {
    val player = players.apply(playerId)
    val table = tables.apply(tableId)
    table.stand(player)

  }

  @throws(classOf[RemoteException])
  @throws(classOf[InsufficientFundsException])
  def doubleDown(playerId: String, tableId: String): Unit = {
    val player = players.apply(playerId)
    val table = tables.apply(tableId)
    table.doubleDown(player)
  }

  @throws(classOf[RemoteException])
  @throws(classOf[InsufficientFundsException])
  def split(playerId: String, tableId: String): Unit = {
    val player = players.apply(playerId)
    val table = tables.apply(tableId)
    table.split(player)
  }

  @throws(classOf[RemoteException])
  def surrender(playerId: String, tableId: String): Unit = {
    val player = players.apply(playerId)
    val table = tables.apply(tableId)
    table.surrender(player)
  }

  @throws(classOf[RemoteException])
  @throws(classOf[InsufficientFundsException])
  def insurance(playerId: String, tableId: String, bet: Int): Unit = {
    val player = players.apply(playerId)
    val table = tables.apply(tableId)
    table.insurance(player, bet)
  }

  @throws(classOf[RemoteException])
  @throws(classOf[TableFullException])
  def join(playerId: String, tableId: String): Boolean = {
    if (!players.contains(playerId))
      players.put(playerId, new Player(playerId))

    val player = players.apply(playerId)
    if (player.inSession)
      false

    val table = tables.apply(tableId)
    player.reset
    table.join(player)
    player.startSession
    true
  }

  @throws(classOf[RemoteException])
  def evaluate(playerId: String, tableId: String): Int = {
    val player = players.apply(playerId)
    val table = tables.apply(tableId)
    table.evaluate(player)
  }

  @throws(classOf[RemoteException])
  def leave(playerId: String, tableId: String): Unit = {
    val player = players.apply(playerId)
    val table = tables.apply(tableId)
    table.leave(player)
    player.closeSession
  }

  @throws(classOf[RemoteException])
  @throws(classOf[InsufficientFundsException])
  def stay(playerId: String, tableId: String, bet: Int): Unit = {
    val player = players.apply(playerId)
    val table = tables.apply(tableId)
    table.stay(player, bet)
  }

  @throws(classOf[RemoteException])
  def getPlayerPoints(playerId: String): Int = {
    val player = players.apply(playerId)
    player.points
  }

  @throws(classOf[RemoteException])
  def getDealerPoints(tableId: String): Int = {
    val table = tables.apply(tableId)
    table.dealerPoints
  }

  @throws(classOf[RemoteException])
  def getPlayerCards(playerId: String): Deck = {
    val player = players.apply(playerId)
    player.hand
  }

  @throws(classOf[RemoteException])
  def getDealerCards(tableId: String): Deck = {
    val table = tables.apply(tableId)
    table.dealer.hand
  }

  @throws(classOf[RemoteException])
  def inSession(playerId: String): Boolean = {
    if (!players.contains(playerId)) {
      false
    } else {
      val player = players.apply(playerId)
      player.inSession
    }
  }

  @throws(classOf[RemoteException])
  def isOver(tableId: String): Boolean = {
    val table = tables.apply(tableId)
    table.over
  }

  @throws(classOf[RemoteException])
  def getTableList(): Array[String] = {
    tables.keySet.toArray
  }

  @throws(classOf[RemoteException])
  def isPlaying(playerId: String): Boolean = {
    val player = players.apply(playerId)
    player.playing
  }

  @throws(classOf[RemoteException])
  def getPlayerList(tableId: String): Array[String] = {
    val table = tables.apply(tableId)
    table.playerIdList
  }

  @throws(classOf[RemoteException])
  def isActive(playerId: String): Boolean = {
    val player = players.apply(playerId)
    player.active
  }

  @throws(classOf[RemoteException])
  def holeRevealed(tableId: String): Boolean = {
    val table = tables.apply(tableId)
    table.holeRevealed
  }

  @throws(classOf[RemoteException])
  def isEvaluated(playerId: String): Boolean = {
    val player = players.apply(playerId)
    player.evaluated
  }

  @throws(classOf[RemoteException])
  def getBet(playerId: String): Int = {
    val player = players.apply(playerId)
    player.bet
  }

  @throws(classOf[RemoteException])
  def getMoney(playerId: String): Int = {
    val player = players.apply(playerId)
    player.money
  }

  @throws(classOf[RemoteException])
  def getInsurance(playerId: String): Int = {
    val player = players.apply(playerId)
    player.insurance
  }

  @throws(classOf[RemoteException])
  def isPlayingSide(playerId: String): Boolean = {
    val player = players.apply(playerId)
    player.playingSide
  }

  @throws(classOf[RemoteException])
  def getSideBet(playerId: String): Int = {
    val player = players.apply(playerId)
    player.sideHand.bet
  }

  @throws(classOf[RemoteException])
  def getSidePoints(playerId: String): Int = {
    val player = players.apply(playerId)
    player.sideHand.points
  }

  @throws(classOf[RemoteException])
  def getSideCards(playerId: String): Deck = {
    val player = players.apply(playerId)
    player.sideHand.hand

  }
}