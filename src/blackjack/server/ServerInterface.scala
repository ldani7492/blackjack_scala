package blackjack.server

import java.rmi.Remote
import java.rmi.RemoteException

import blackjack.Deck
import blackjack.InsufficientFundsException
import blackjack.TableFullException

trait ServerInterface extends Remote {
  @throws(classOf[RemoteException])
  def hit(playerId: String, tableId: String): Unit

  @throws(classOf[RemoteException])
  def stand(playerId: String, tableId: String): Unit

  @throws(classOf[RemoteException])
  @throws(classOf[InsufficientFundsException])
  def doubleDown(playerId: String, tableId: String): Unit

  @throws(classOf[RemoteException])
  @throws(classOf[InsufficientFundsException])
  def split(playerId: String, tableId: String): Unit

  @throws(classOf[RemoteException])
  @throws(classOf[InsufficientFundsException])
  def insurance(playerId: String, tableId: String, bet: Int): Unit

  @throws(classOf[RemoteException])
  def surrender(playerId: String, tableId: String): Unit

  @throws(classOf[RemoteException])
  @throws(classOf[TableFullException])
  def join(playerId: String, tableId: String): Boolean

  @throws(classOf[RemoteException])
  def leave(playerId: String, tableId: String): Unit

  @throws(classOf[RemoteException])
  @throws(classOf[InsufficientFundsException])
  def stay(playerId: String, tableId: String, bet: Int)

  @throws(classOf[RemoteException])
  def getPlayerPoints(playerId: String): Int

  @throws(classOf[RemoteException])
  def getDealerPoints(id: String): Int

  @throws(classOf[RemoteException])
  def getPlayerCards(playerId: String): Deck

  @throws(classOf[RemoteException])
  def getDealerCards(id: String): Deck

  @throws(classOf[RemoteException])
  def inSession(id: String): Boolean

  @throws(classOf[RemoteException])
  def isOver(id: String): Boolean

  @throws(classOf[RemoteException])
  def evaluate(playerId: String, tableId: String): Int

  @throws(classOf[RemoteException])
  def getTableList(): Array[String]

  @throws(classOf[RemoteException])
  def isPlaying(playerId: String): Boolean

  @throws(classOf[RemoteException])
  def getPlayerList(tableId: String): Array[String]

  @throws(classOf[RemoteException])
  def isActive(playerId: String): Boolean

  @throws(classOf[RemoteException])
  def holeRevealed(id: String): Boolean

  @throws(classOf[RemoteException])
  def isEvaluated(playerId: String): Boolean

  @throws(classOf[RemoteException])
  def getBet(playerId: String): Int

  @throws(classOf[RemoteException])
  def getMoney(playerId: String): Int

  @throws(classOf[RemoteException])
  def getInsurance(playerId: String): Int

  @throws(classOf[RemoteException])
  def isPlayingSide(playerId: String): Boolean

  @throws(classOf[RemoteException])
  def getSideBet(playerId: String): Int

  @throws(classOf[RemoteException])
  def getSidePoints(playerId: String): Int

  @throws(classOf[RemoteException])
  def getSideCards(playerId: String): Deck
}