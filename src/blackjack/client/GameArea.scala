package blackjack.client

import java.awt.Color
import java.awt.Graphics2D
import java.rmi.RemoteException

import scala.collection.mutable.ArrayBuffer
import scala.swing.Panel

import blackjack.Card

@SerialVersionUID(5060593748000544515L)
class GameArea(private val gui: GUI) extends Panel {

  private var players = List.empty[String]

  override def paintComponent(g: Graphics2D): Unit = {
    super.paintComponent(g)

    g.setColor(Color.WHITE)

    setPlayerList

    try {
      if (!players.isEmpty)
        drawPlayers(g, 0, players)

      g.setColor(Color.WHITE)
      val ppoints = String.valueOf(gui.server.getPlayerPoints(gui.playerId))
      val dpoints = String.valueOf(gui.server.getDealerPoints(gui.tableId))

      if (gui.server.isPlayingSide(gui.playerId)) {
        val sidepoints = gui.server.getSidePoints(gui.playerId)
        g.drawString("Player points: " + ppoints + " Dealer points: "
          + dpoints + " Side hand points: " + sidepoints, 20, 400)
      } else
        g.drawString("Player points: " + ppoints + " Dealer points: "
          + dpoints, 20, 400)
      val money = String.valueOf(gui.server.getMoney(gui.playerId))
      val bet = String.valueOf(gui.server.getBet(gui.playerId))
      val insurance = String.valueOf(gui.server.getInsurance(gui.playerId))
      g.drawString("Money: " + money, 20, 420)
      g.drawString("Bet: " + bet, 20, 440)
      g.drawString("Insurance: " + insurance, 20, 460)
      if (gui.server.isPlayingSide(gui.playerId)) {
        val sideBet = String.valueOf(gui.server.getSideBet(gui.playerId))
        g.drawString("Side bet: " + sideBet, 20, 480)
      }

    } catch {
      case re: RemoteException => {
        re.printStackTrace
      }
    }
  }

  def drawPlayers(g: Graphics2D, i: Int, players: List[String]): Unit = {
    val playerId = players.head

    g.setColor(Color.WHITE)
    g.drawString(playerId + "'s cards: ", 20, 20 + 75 * i)

    val cards = gui.server.getPlayerCards(playerId).cards

    drawCards(cards, i, 0, g)

    if (gui.server.isPlayingSide(playerId)) {
      val side = gui.server.getSideCards(playerId).cards
      drawCards(side, i, 8, g)
    }

    if (!players.tail.isEmpty)
      drawPlayers(g, i + 1, players.tail)
    else
      drawDealer(g, i + 1)
  }

  def drawDealer(g: Graphics2D, i: Int): Unit = {
    g.setColor(Color.WHITE);
    g.drawString("Dealer's cards: ", 20, 20 + 75 * i);

    val cards = gui.server.getDealerCards(gui.tableId).cards

    if (gui.server.holeRevealed(gui.tableId))
      drawCards(cards, i, 0, g)
    else if (cards.size != 0) {
      drawCard(cards.head, i, 0, g)
      g.setColor(Color.BLACK);
      g.fillRect(20 + 32, 30 + 75 * i, 30, 40);
      g.setColor(Color.WHITE);
      g.drawString("hole", 25 + 32, 50 + 75 * i);
    }

  }

  def drawCards(cards: ArrayBuffer[Card], i: Int, j: Int, g: Graphics2D): Unit = {
    if (!cards.isEmpty) {
      val card = cards.head

      drawCard(card, i, j, g)

      if (!cards.tail.isEmpty)
        drawCards(cards.tail, i, j + 1, g)
    }
  }

  def drawCard(card: Card, i: Int, j: Int, g: Graphics2D): Unit = {
    g.setColor(Color.WHITE)
    g.fillRect(20 + 32 * j, 30 + 75 * i, 30, 40)
    g.setColor(Color.BLACK)
    g.drawRect(20 + 32 * j, 30 + 75 * i, 30, 40)
    g.setColor(card.color)
    g.drawString(card.toString, 25 + 32 * j, 50 + 75 * i)
  }

  def setPlayerList: Unit = {
    try {
      players = gui.server.getPlayerList(gui.tableId).toList
    } catch {
      case re: RemoteException => {
        Console.err.println("Failed to get player list")
        re.printStackTrace
      }
    }
  }

}