package blackjack.client

import scala.swing.MainFrame
import blackjack.server.ServerInterface
import scala.swing.Button
import java.awt.event.ActionListener
import scala.swing.Swing
import javax.swing.Timer
import java.rmi.RemoteException
import java.awt.Dimension
import java.awt.Point
import java.awt.Color
import javax.swing.BorderFactory
import scala.swing.FlowPanel
import scala.swing.BorderPanel
import blackjack.InsufficientFundsException
import scala.swing.Dialog
import blackjack.Deck
import blackjack.CardRank

@SerialVersionUID(-1402252779303804232L)
class GUI(private val _server: ServerInterface, private val _playerId: String, private val _tableId: String, private val _client: BlackjackClient) extends MainFrame {

  private val hit = new Button("Hit")
  private val stand = new Button("Stand")
  private val doubleDown = new Button("Double")
  private val split = new Button("Split")
  private val surrender = new Button("Surrender")
  private val insurance = new Button("Insurance")
  private val gameArea = new GameArea(this)

  val buttons = new FlowPanel(hit, stand, doubleDown, split, surrender, insurance)

  contents = new BorderPanel {
    layout(gameArea) = BorderPanel.Position.Center
    layout(buttons) = BorderPanel.Position.South
  }

  @volatile
  private var over = server.isOver(tableId)

  private val refreshTimer = new Timer(100, Swing.ActionListener(ea => {
    refresh
  }))

  private val timeout = new Timer(60000, Swing.ActionListener(ea => {
    try {
      server.leave(playerId, tableId);
    } catch {
      case e1: Exception => e1.printStackTrace
    }
  }))

  private var playing = List.empty[String]

  refreshTimer.start

  override def closeOperation: Unit = {
    try {
      if (server.inSession(playerId)) {
        server.leave(playerId, tableId)
        System.out.println("closing")
      }
      super.closeOperation

    } catch {
      case re: RemoteException => re.printStackTrace
    }
  }

  title = "BlackJack"

  size = new Dimension(600, 600)
  location = new Point(500, 200)

  gameArea.background = new Color(0, 200, 0)
  gameArea.border = BorderFactory.createLineBorder(Color.black)

  hit.action = swing.Action("Hit") {
    try {
      timeout.restart
      server.hit(playerId, tableId)
      refresh
    } catch {
      case re: RemoteException => {
        System.err.println("Connection error")
        re.printStackTrace
      }
    }
  }

  stand.action = swing.Action("Stand") {
    try {
      timeout.stop
      timeout.setDelay(10000)
      server.stand(playerId, tableId)
      refresh
    } catch {
      case re: RemoteException => {
        System.err.println("Connection error")
        re.printStackTrace
      }
    }
  }

  split.action = swing.Action("Split") {
    try {
      timeout.stop
      timeout.setDelay(10000)
      server.split(playerId, tableId)
      refresh
    } catch {
      case re: RemoteException => {
        System.err.println("Connection error")
        re.printStackTrace
      }
      case ife: InsufficientFundsException => {
        Dialog.showConfirmation(message = "You don't have enough money to do that.",
          title = "BlackJack",
          optionType = Dialog.Options.Default)
      }
    }
  }

  doubleDown.action = swing.Action("Double") {
    try {
      timeout.stop
      timeout.setDelay(10000)
      server.doubleDown(playerId, tableId)
      refresh
    } catch {
      case re: RemoteException => {
        System.err.println("Connection error")
        re.printStackTrace
      }
      case ife: InsufficientFundsException => {
        Dialog.showConfirmation(message = "You don't have enough money to do that.",
          title = "BlackJack",
          optionType = Dialog.Options.Default)
      }
    }
  }

  surrender.action = swing.Action("Surrender") {
    try {
      timeout.stop
      timeout.setDelay(10000)
      server.surrender(playerId, tableId)
      refresh
    } catch {
      case re: RemoteException => {
        System.err.println("Connection error")
        re.printStackTrace
      }
    }
  }

  insurance.action = swing.Action("Insurance") {
    try {
      timeout.restart
      val bet = BlackjackClient.getBet
      server.insurance(playerId, tableId, bet)
      refresh
    } catch {
      case re: RemoteException => {
        System.err.println("Connection error")
        re.printStackTrace
      }
      case ife: InsufficientFundsException => {
        Dialog.showConfirmation(message = "You don't have enough money to do that.",
          title = "BlackJack",
          optionType = Dialog.Options.Default)
      }
    }
  }

  visible = true

  def refresh: Unit = {
    try {
      if (!server.inSession(playerId)) {
        Dialog.showConfirmation(message = "Your session ended.",
          title = "BlackJack",
          optionType = Dialog.Options.Default)
        System.exit(0)
      } else {
        gameArea.repaint
        if (!over) {
          inProgress
        } else {
          gameOver
        }
      }
    } catch {
      case re: RemoteException => {
        System.err.println("Connection error")
        re.printStackTrace
      }
    }
  }

  @throws(classOf[RemoteException])
  private def inProgress: Unit = {
    over = server.isOver(tableId)

    if (over || !server.isActive(playerId)
      || !playing.contains(playerId)) {
      justFinished
    } else {
      inGame
    }
    if (server.isActive(playerId)) {
      timeout.start
    }
  }

  @throws(classOf[RemoteException])
  private def gameOver: Unit = {
    if (server.getPlayerPoints(playerId) == 21
      && !server.isEvaluated(playerId)) {
      disableAll
      refreshTimer.stop
      client.evaluate
    }

    newGame
  }

  @throws(classOf[RemoteException])
  private def justFinished: Unit = {
    disableAll

    if (playing != null && playing.contains(playerId) && over) {
      refreshTimer.stop
      client.evaluate
    }
  }

  @throws(classOf[RemoteException])
  private def inGame: Unit = {
    hit.enabled = true
    stand.enabled = true
    doubleDown.enabled = true
    if (server.getPlayerCards(playerId).size == 2) {
      checkSurrender
      val dealer = server.getDealerCards(tableId)
      val player = server.getPlayerCards(playerId)
      checkInsurance(dealer)
      checkSplit(player)
    } else {
      doubleDown.enabled = false
      split.enabled = false
      surrender.enabled = false
      insurance.enabled = false
    }
    checkForBlackjack
  }

  @throws(classOf[RemoteException])
  private def checkForBlackjack: Unit = {
    if (server.getPlayerPoints(playerId) == 21 &&
      !server.isPlayingSide(playerId))
      server.stand(playerId, tableId)
  }

  @throws(classOf[RemoteException])
  private def checkSplit(player: Deck): Unit = {
    if (!server.isPlayingSide(playerId) &&
      (player.get(0).value == player.get(1).value))
      split.enabled = true
    else
      split.enabled = false
  }

  @throws(classOf[RemoteException])
  private def checkInsurance(dealer: Deck): Unit = {
    if (dealer.get(0).rank == CardRank.ACE
      && server.getInsurance(playerId) == 0) {
      insurance.enabled = true
    } else
      insurance.enabled = false
  }

  @throws(classOf[RemoteException])
  private def checkSurrender: Unit = {
    if (!server.isPlayingSide(playerId))
      surrender.enabled = true
    else
      surrender.enabled = false
  }

  private def disableAll: Unit = {
    hit.enabled = false
    stand.enabled = false
    doubleDown.enabled = false
    surrender.enabled = false
    insurance.enabled = false
  }

  def newGame: Unit = {
    try {
      over = server.isOver(tableId)
      refreshTimer.restart
      playing = server.getPlayerList(tableId).toList
    } catch {
      case re: RemoteException => re.printStackTrace
    }
  }

  def server = _server
  def playerId = _playerId
  def tableId = _tableId
  def client = _client
}