package blackjack.client

import java.rmi.registry.Registry
import java.rmi.registry.LocateRegistry
import blackjack.server.ServerInterface
import scala.swing._
import java.rmi.NotBoundException
import java.rmi.RemoteException
import blackjack.TableFullException
import blackjack.InsufficientFundsException
import scala.language.postfixOps

class BlackjackClient(private val _server: ServerInterface, private val _playerId: String, private val _tableId: String) {
  private val _gui: GUI = new GUI(_server, _playerId, _tableId, this)

  @throws(classOf[RemoteException])
  @throws(classOf[InsufficientFundsException])
  @throws(classOf[NotBoundException])
  def newGame(bet: Int): Unit = {
    _server.stay(_playerId, _tableId, bet)
    _gui.newGame
  }

  @throws(classOf[RemoteException])
  def evaluate: Unit = {
    if (!_server.inSession(_playerId)) {
      Dialog.showConfirmation(message = "Your session ended.",
        title = "BlackJack",
        optionType = Dialog.Options.Default)
      System.exit(0)
    } else {
      val win = _server.evaluate(_playerId, _tableId)

      Dialog.showConfirmation(message = "You won " + win + " amount of money. Would you like to start a new game?",
        title = "BlackJack",
        optionType = Dialog.Options.YesNo) match {
          case Dialog.Result.Yes => {
            placeBet
          }
          case Dialog.Result.No => {
            _server.leave(_playerId, _tableId)
            _gui.visible_=(false)
            _gui.dispose
            System.out.println("closing")
          }
        }
    }
  }

  def placeBet: Unit = {
    val bet = BlackjackClient.getBet

    if (bet < 0)
      System.exit(0)

    try {
      newGame(bet);
    } catch {
      case ife: InsufficientFundsException => {
        Dialog.showConfirmation(message = "You don't have enough money to do that.",
          title = "BlackJack",
          optionType = Dialog.Options.Default)
        _server.leave(_playerId, _tableId)
        System.exit(0)
      }
    }
  }
}

object BlackjackClient {

  private var _client: BlackjackClient = _

  def main(args: Array[String]): Unit = {

    val address = args.apply(0)

    try {
      initialize(address)

      _client.placeBet

    } catch {
      case re: RemoteException => re printStackTrace
      case nbe: NotBoundException => nbe printStackTrace
    }
  }

  @throws(classOf[RemoteException])
  @throws(classOf[NotBoundException])
  def initialize(address: String): Unit = {
    val registry = LocateRegistry getRegistry (address)

    val server = registry.lookup("BlackjackServer").asInstanceOf[ServerInterface]
    println("connected to " + address)

    val playerId = enterName(server)
    if (playerId == null)
      System.exit(0)

    val tableId = joinTable(server, playerId)

    _client = new BlackjackClient(server, playerId, tableId)
  }

  def enterName(server: ServerInterface): String = {
    val playerId = Dialog.showInput[String](message = "Please enter your name:",
      title = "Blackjack",
      initial = null).get

    if (playerId == null)
      System.exit(0)

    if (server.inSession(playerId)) {
      Dialog.showConfirmation(message = "Can't join: you already have an active session.",
        title = "BlackJack",
        optionType = Dialog.Options.Default)
      null
    } else playerId
  }

  def joinTable(server: ServerInterface, playerId: String): String = {
    val tables = server.getTableList

    pickJoin(tables, server, playerId)
  }

  def pickJoin(tables: Array[String], server: ServerInterface, playerId: String): String = {
    val tableId = Dialog.showInput[String](message = "Please choose a table:",
      title = "Blackjack",
      entries = tables,
      initial = tables.apply(0)).get

    if (tableId == null)
      System.exit(0)

    try {
      server.join(playerId, tableId);
      println("Joined " + tableId + " as " + playerId)
      tableId

    } catch {
      case tfe: TableFullException => {
        Dialog.showConfirmation(message = "Table is full. Please choose a different one",
          title = "BlackJack",
          optionType = Dialog.Options.Default)
        pickJoin(tables, server, playerId)
      }
    }
  }

  def getBet: Int = {
    val betStr = Dialog.showInput[String](message = "Please place your bet:",
      title = "Blackjack",
      initial = null).get

    try {
      val bet = betStr.toInt

      if (bet > 0)
        bet
      else
        getBet
    } catch {
      case nfe: NumberFormatException => {
        -1
      }
      case nsee: NoSuchElementException => {
        -1
      }
    }

  }
}