package blackjack.test

import blackjack.Player
import blackjack.Table

object SpeedTest {
  def main(args: Array[String]): Unit = {
    val startTime = System.currentTimeMillis();
    
    (1 to 1000) foreach (x => play)
    
    val stopTime = System.currentTimeMillis();
    val elapsedTime = stopTime - startTime;
    
    System.out.println(elapsedTime);
  }
  
  def play {
    val table = new Table("TestTable", 6)
    val player1 = new Player("Player 1")
    val player2 = new Player("Player 2")
    val player3 = new Player("Player 3")
    
    player1.startSession
    player2.startSession
    player3.startSession

      table.join(player1)
      table.join(player2)
      table.join(player3)
      
      table.stay(player1, 100)
      table.stay(player2, 100)
      table.stay(player3, 100)
      
      table.newGame
      
      while (!table.over) {
        table.stand(table.activePlayer);
      }

  }
}