package blackjack

import util.Random
import scala.collection.mutable.ArrayBuffer

@SerialVersionUID(5540832176342824311L)
class Deck extends Serializable {
  private var _cards = ArrayBuffer.empty[Card]

  @throws(classOf[EmptyDeckException])
  def draw: Card = {
    if (_cards.length == 0)
      throw new EmptyDeckException
    else {
      val card = _cards.head
      _cards.remove(0)
      card
    }
  }

  def shuffle: Unit = {
    _cards = Random.shuffle(_cards)
  }

  def clear: Unit = {
    _cards.clear
  }

  def add(card: Card): Unit = {
    _cards += card
  }

  def size: Int = _cards.length

  def get(i: Int): Card = {
    _cards.apply(i)
  }

  override def toString: String = {
    var builder = new StringBuilder
    for (card <- _cards) {
      builder.append(card.toString())
      builder.append(" ")
    }
    builder.toString
  }

  def cards = _cards
}